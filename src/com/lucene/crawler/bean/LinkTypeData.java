package com.lucene.crawler.bean;

public class LinkTypeData {

	private int id;

	// 链接的地址
	private String linkHref;
	// 链接的标题
	private String linkTitle;
	// 摘要
	private String contentAbstract;
	// 内容
	private String content;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLinkHref() {
		return linkHref;
	}

	public void setLinkHref(String linkHref) {
		this.linkHref = linkHref;
	}

	public String getLinkTitle() {
		return linkTitle;
	}

	public void setLinkTitle(String linkTitle) {
		this.linkTitle = linkTitle;
	}

	public String getContentAbstract() {
		return contentAbstract;
	}

	public void setContentAbstract(String contentAbstract) {
		this.contentAbstract = contentAbstract;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "LinkTypeData [id=" + id + ", linkHref=" + linkHref + ", linkTitle=" + linkTitle + ", contentAbstract="
				+ contentAbstract + ", content=" + content + "]";
	}

}
