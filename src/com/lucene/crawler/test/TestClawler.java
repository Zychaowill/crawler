package com.lucene.crawler.test;

import java.util.List;

import org.junit.Test;

import com.lucene.crawler.bean.LinkTypeData;
import com.lucene.crawler.core.GrabService;
import com.lucene.crawler.rule.Rule;

public class TestClawler {
	
	
	@Test
	public void testGetDataFromJindDong() {
		Rule rule = new Rule("http://search.jd.com/Search?keyword=Python&enc=utf-8&book=y&wq=Python&pvid=33xo9lni.p4a1qb",
				new String[]{"keyword"}, new String[]{"Python"}, "div[class=p-name]", Rule.CLASS, Rule.POST);
		
		List<LinkTypeData> datas = GrabService.grab(rule);
		for (LinkTypeData linkTypeData : datas) {
			System.out.println(linkTypeData);
		}
	}
}
